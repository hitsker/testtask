﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartButton : MonoBehaviour
{
    [SerializeField] Button startButton;

    private void Start()
    {
        startButton.onClick.AddListener(() =>
        {
            GameManager.instance.player.ResetSpeed();
            this.gameObject.SetActive(false);
        });
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
