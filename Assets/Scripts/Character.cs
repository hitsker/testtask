﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] Animator animator;
    [SerializeField] Gun gun;

    private float savedSpeed;
    private bool allowShooting = false;
    private bool isAlive = true;
    private Rigidbody rigitbody;

    public void Start()
    {
        savedSpeed = speed;
        speed = 0;
        rigitbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0) && allowShooting)
        {
            gun.Shoot();
        }

#endif
#if !UNITY_EDITOR
        if (Input.GetTouch(0).phase == TouchPhase.Began && allowShooting)
        {
            gun.Shoot();
        }
#endif
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void AnimationEnd()
    {
        ResetSpeed();
    }

    public void ResetSpeed()
    {
        speed = savedSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "FinishTrigger")
        {
            GameManager.instance.fbManager.LogAchieveLevelEvent(SceneManager.GetActiveScene().buildIndex.ToString());
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else if (other.gameObject.tag == "AnimationTrigger")
        {
            PlayAnimation(other.GetComponent<Trigger>().GetAnimationName());
            Time.timeScale = 0.2f;
        }

    }

    public void PlayAnimation(string animationName)
    {
        speed = 0;
        allowShooting = true;
        animator.SetTrigger(animationName);
    }

    public void EndAnimation()
    {
        if (isAlive)
        {
            ResetSpeed();
        }
        allowShooting = false;
        transform.rotation = Quaternion.identity;
        Time.timeScale = 1f;

    }

    public void IsDead()
    {
        if (isAlive)
        {
            animator.enabled = false;
            rigitbody.useGravity = true;
            rigitbody.isKinematic = false;
            rigitbody.freezeRotation = false;
            speed = 0;
            Time.timeScale = 1f;
            GameManager.instance.restartButton.Show();
            isAlive = false;
        }
    }
}
