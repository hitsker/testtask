﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Trigger : MonoBehaviour
{
    [SerializeField] string animationName;

    public string GetAnimationName()
    {
        return animationName;
    }
}
