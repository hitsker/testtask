﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed;
    
    private Vector3 direction = Vector3.right;

    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
    }

    private void Start()
    {
        DestroyBullet();
    }

    void Update()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy" && tag == "CharacterBullet")
        {
            other.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(100, 100, 0), other.transform.position);
        }
    }

    public void DestroyBullet()
    {
        StartCoroutine(SelfDestruction());
    }

    public IEnumerator SelfDestruction()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
    }
}
