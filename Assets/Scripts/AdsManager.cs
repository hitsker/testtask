﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager
{

    public void Setup()
    {
        IronSource.Agent.init("b22c64a5");
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.TOP);
        IronSource.Agent.displayBanner();
    }

    public void ShowRewardedVideo()
    {
        IronSource.Agent.showRewardedVideo();
    }
}
