﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private ParticleSystem EnemyPeaces;
    [SerializeField] private Bullet bullet;

    private bool isAlive=true;
    private bool shoulShoot = true; 

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "CharacterBullet")
        {
            isAlive = false;
        }
    }

    public void DelayedShoot()
    {
        if (shoulShoot)
        {
            StartCoroutine(Shoot());
            shoulShoot = false;
        }
    }

    public IEnumerator Shoot()
    {
        yield return new WaitForSeconds(0.3f);
        if (isAlive)
        {
            Debug.Log("Shoot");
            Bullet bulletInstance= Instantiate(bullet, transform.position, Quaternion.identity);
            bulletInstance.tag = "EnemyBullet";
            bulletInstance.SetDirection(DetermineDirection());
            GameManager.instance.player.IsDead();
        }
    }

    public Vector3 DetermineDirection()
    {
        GameObject target = GameManager.instance.player.gameObject;
        Vector3 heading = target.transform.position - transform.position;
        float distance = heading.magnitude;
        Vector3 direction = heading / distance;
        return direction;
    }
}
