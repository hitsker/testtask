﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] Bullet bullet;
    [SerializeField] GameObject ray;

    private void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, 10, layerMask:1<<9))
        {
            hit.collider.gameObject.GetComponent<Enemy>().DelayedShoot();
        }
    }

    public void Shoot()
    {
        var instance = GameManager.instance.bulletPool.Instantiate(transform.position, Quaternion.identity);
        //Bullet bulletInstance = Instantiate(bullet, transform.position, Quaternion.identity);
        //bulletInstance.transform.rotation = transform.rotation;
        instance.transform.rotation = transform.rotation;
        instance.tag = "CharacterBullet";
        //bulletInstance.tag = "CharacterBullet";
    }

    public void ShowRay()
    {
        ray.gameObject.SetActive(true);
    }

    public void HideRay()
    {
        ray.gameObject.SetActive(false);
    }
}
