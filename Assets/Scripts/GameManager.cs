﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    
    public Character player;
    public RestartButton restartButton;
    public Pool bulletPool;

    public FBmanager fbManager;
    public AdsManager adsManager;

    private void Awake()
    {
        instance = this;
        player = GameObject.Find("Player").GetComponent<Character>();
        restartButton = GameObject.Find("RestartButton").GetComponent<RestartButton>();
        bulletPool = GameObject.Find("BulletPool").GetComponent<Pool>();

        fbManager = new FBmanager();
        fbManager.Setup();
        adsManager = new AdsManager();
        adsManager.Setup();

    }

    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }
}
