﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffect : MonoBehaviour
{
    [SerializeField] float liveTime;
    void Start()
    {
        Die(liveTime);
    }

    public IEnumerator Die(float liveTime)
    {
        yield return new WaitForSeconds(liveTime);
        Destroy(this.gameObject);
    }
}
